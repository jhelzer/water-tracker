//
//  WaterTableViewCell.swift
//  Water Tracker
//
//  Created by Wendell Beverly on 6/11/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit

class WaterTableViewCell: UITableViewCell {

    
    static let reuseIdentifier = "WaterCell"
    
    @IBOutlet var waterDrank: UILabel!
    @IBOutlet var loggedTime: UILabel!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
