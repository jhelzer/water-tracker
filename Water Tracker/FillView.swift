//
//  FillView.swift
//  Water Tracker
//
//  Created by Jordan Helzer on 5/16/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit

class FillView: UIView {

	fileprivate var _imageView: UIImageView?
	fileprivate var _fill: Float = 0
	private var fill: Float {
		get {
			return _fill
		}
		set {
			_fill = newValue
			if newValue > 1.0 {
				_fill = 1.0
			} else if newValue < 0.0 {
				_fill = 0.0
			}
		}
	}
	
	func fill(to fillAmount: Float, animated: Bool = false) {
		var oldFill = fill
		fill = fillAmount
		let difference = fill - oldFill
		
		if difference > 0 && animated {
			let step = difference / 15.0
			var images: [UIImage] = []
			while oldFill <= fill {
				oldFill = oldFill + step
				print("\(oldFill)")
				images.append(WTStyleKit.imageOfFigure(percentFull: CGFloat(oldFill)))
			}
			
			_imageView?.animationImages = images
			_imageView?.animationDuration = 0.5
			_imageView?.animationRepeatCount = 1
			_imageView?.startAnimating()
			self.perform(#selector(afterAnimation), with: nil, afterDelay: 0.5)
		}
	}
	
	@objc private func afterAnimation() {
		_imageView?.image = _imageView?.animationImages?.last
		_imageView?.stopAnimating()
		_imageView?.animationImages = nil
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		_imageView = UIImageView()
		_imageView?.translatesAutoresizingMaskIntoConstraints = false
		addSubview(_imageView!)
		_imageView?.constrainToSuperview()
		_imageView?.contentMode = .scaleAspectFit
		_imageView?.image = WTStyleKit.imageOfFigure(percentFull: CGFloat(fill))
	}
	
	

}
