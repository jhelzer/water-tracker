//
//  MainTabBarViewController.swift
//  Water Tracker
//
//  Created by Jordan Helzer on 5/16/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit
import FontAwesomeKit

fileprivate enum TabTitles: String, CustomStringConvertible {
	case Hydrate
	case History
	case Achievements
	case Settings
	
	fileprivate var description: String {
		return self.rawValue
	}
}

class MainTabBarViewController: UITabBarController {

	private var tabIcons = [
		TabTitles.Hydrate : "ion-waterdrop",
		TabTitles.History : "ion-stats-bars",
		TabTitles.Achievements : "ion-android-star",
		TabTitles.Settings : "ion-ios-gear"
	]
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		if let tabBarItems = tabBar.items {
			for item in tabBarItems {
				if let title = item.title, let tab = TabTitles(rawValue: title), let glyph = tabIcons[tab] {
					let icon: FAKIonIcons?
					do {
						icon = try FAKIonIcons(identifier: glyph, size: 30)
						if let icon = icon {
							item.image = icon.image(with: CGSize(width: 30, height: 30))
							icon.addAttributes([NSForegroundColorAttributeName : UIColor(red: 0, green: 131.0/255.0, blue: 218.0/255.0, alpha: 1.0)])
							item.selectedImage = icon.image(with: CGSize(width: 30, height: 30))
						}
					} catch let error as NSError {
						print(error.localizedDescription)
					}
					
				}
			}
		}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
