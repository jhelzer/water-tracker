//
//  HydrateViewController.swift
//  Water Tracker
//
//  Created by Jordan Helzer on 5/8/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit
import CoreData

class HydrateViewController: UIViewController {

	@IBOutlet weak var fillView: FillView!
	
	var dailyGoal: Float = 75
	var amountDrank: Float = 0.0
    
    var waterIntakes = [NSManagedObject]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		fillView.fill(to: amountDrank / dailyGoal)
        
        self.pullWaterData()
        
	}
	
	func addDrink(_ drank: Float) {
		amountDrank = amountDrank + drank
		self.fillView.fill(to: self.amountDrank / self.dailyGoal, animated: true)
	}
    
    
    @IBAction func myButton(_ sender: Any) {
        print ("Made it")
    }
    
    
    
	@IBAction func addButtonPressed(_ sender: Any) {
        
		let amounts: [Float] = [8.0, 16.9, 24.0, 32.0]
		let alert = UIAlertController(title: "Add Some Drink", message: "Select the amount that you would like to add", preferredStyle: .actionSheet)
		
		for amount in amounts {
			alert.addAction(UIAlertAction(title: "\(String(format: "%.1f", amount)) Fl Oz", style: .default, handler: { (action) in
				self.addDrink(amount)
                self.addWaterDrankToDB(amount: amount)
			}))
	
		} // End For
		
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		present(alert, animated: true, completion: nil)
	}
    
    
    func addWaterDrankToDB( amount: Float){
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDel.managedObjectContext
        
        let entity = NSEntityDescription.entity(forEntityName: "DailyLog", in: managedContext)
        
        let newEntry = NSManagedObject(entity: entity!, insertInto: managedContext)
        
        let today:Date = Date()
        
        newEntry.setValue(amount, forKey: "waterAmount")
        
        newEntry.setValue(today, forKey: "logDateTime")
        
        do {
            
            try managedContext.save()
        }
        catch let error as NSError {
            
            print("FAILED! %@", error)
            
        }
        
    }
    
    
    func pullWaterData() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let today:Date = Date()
        
        let formatToday = DateFormatter()
        
        formatToday.dateFormat = "MM/dd/yyyy"
        
        let newStartDate = "\(formatToday.string(from: today)) 00:00"
        
        let newEndDate = "\(formatToday.string(from: today)) 23:59"
        
        let formatLongDate = DateFormatter()
        
        formatLongDate.dateFormat = "MM/dd/yyyy HH:mm"
        
        formatLongDate.timeZone = TimeZone(abbreviation: "PST")
        
        let startDate:Date = formatLongDate.date(from: newStartDate)!
        
        let endDate:Date = formatLongDate.date(from: newEndDate)!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DailyLog")
        
        let predicate = NSPredicate(format: "(logDateTime >= %@) AND (logDateTime <= %@)", startDate as CVarArg, endDate as CVarArg)
        
        fetchRequest.predicate = predicate
        
        var waterFromData:Float = 0.0
  
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            
            waterIntakes = results as! [NSManagedObject]
            
            if waterIntakes.count > 0 {
                
                for waterIntake in waterIntakes {
                    
                    let someDate:Date = waterIntake.value(forKey: "logDateTime") as! Date
                    let someAmount:Int16 = waterIntake.value(forKey: "waterAmount") as! Int16
                    
                    print("we drank \(someAmount) oz of water on \(someDate)")
                    
                    let thisWater = waterIntake.value(forKey: "waterAmount") as! Float
                    
                    waterFromData = waterFromData + thisWater
                    
                }
            
            }
            
        }catch let error as NSError{
            
            print(error)
            
        }

        if waterFromData > 0.0 {
            
            self.addDrink(waterFromData)
            
        }

        
    }
    

}

