//
//  HistoryViewController.swift
//  Water Tracker
//
//  Created by Jordan Helzer on 5/8/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    
    var waterLogs = [DailyLog]() {
        
        didSet {
            updateView()
        }
        
        
    }
    
    @IBOutlet weak var waterView: UITableView!
    
    
	override func viewDidLoad() {
		super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.persistentContainer.loadPersistentStores {(persistneStoreDescription, error) in
            if let error  = error {
                print("Unable to Load Persisten Store")
                print("\(error), \(error.localizedDescription)")
            }else {
                self.setupView()
            }
        }
        
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
    

    private func updateView() {
        
        let hasLogs = waterLogs.count > 0
        
        waterView.isHidden = !hasLogs
        
    }
    
    private func setupView() {
        
        updateView()
    }

}

