//
//  UIView+extension.swift
//  Water Tracker
//
//  Created by Jordan Helzer on 5/22/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit

extension UIView {
	func constrainToSuperview() {
		translatesAutoresizingMaskIntoConstraints = false
		let attributes: [NSLayoutAttribute] = [.leading, .trailing, .top, .bottom]
		attributes.forEach { attribute in
			NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: self.superview, attribute: attribute, multiplier: 1, constant: 0).isActive = true
		}
	}
}
