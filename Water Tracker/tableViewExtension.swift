//
//  tableViewExtension.swift
//  Water Tracker
//
//  Created by Wendell Beverly on 6/13/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import Foundation
import UIKit

extension HistoryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return waterLogs.count
    }
    
    func tableView(_ waterView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = waterView.dequeueReusableCell(withIdentifier: "WaterCell", for: indexPath) as? WaterTableViewCell else {
            fatalError("Unexpected Index Path")
        }

        let waterLog = waterLogs[indexPath.row]

        let dateFormater = DateFormatter()

        dateFormater.dateFormat = "MM/dd/yyyy HH:mm"

        let logTime = dateFormater.string(from: waterLog.logDateTime! as Date)

        cell.loggedTime.text = logTime
        cell.waterDrank.text = String(waterLog.waterAmount)
        
        return cell
        
    }

    
    
    
    
    
    
}
